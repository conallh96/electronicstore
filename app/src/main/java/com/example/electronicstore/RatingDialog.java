package com.example.electronicstore;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;


public class RatingDialog extends AppCompatDialogFragment {
    private RatingBar ratingBar;
    private EditText reviewText;
    private RatingDialogListener listener;
    private Product product;

    public RatingDialog(Product product) {
        this.product = product;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.rating_dialog, null);

        if (!(product == null)) {


            builder.setView(view)
                    .setTitle("Rate " + product.getName())
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            double stars = ratingBar.getRating();
                            String opinion = reviewText.getText().toString();
                            listener.applyReview(stars, opinion);
                        }
                    });

            ratingBar = view.findViewById(R.id.ratingBar);
            reviewText = view.findViewById(R.id.reviewBox);

        }
            return builder.create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (RatingDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement ExampleDialogListener");
        }
    }

    public interface RatingDialogListener {
        void applyReview(double stars, String review);
    }
}
