package com.example.electronicstore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ViewOrder extends AppCompatActivity implements RatingDialog.RatingDialogListener{

    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private OrderSheet cart = new OrderSheet();
    private OrderSheet view = new OrderSheet();
    private ArrayList<Product> displayedProducts = new ArrayList<Product>();
    private Product selectedProduct;
    private Customer user;
    private Review postedReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order);

        Intent intent = getIntent();

        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        new GetCustomerInfo().execute();

        Gson g = new Gson();
        String orderString = intent.getStringExtra("Order");
        cart = g.fromJson(orderString, OrderSheet.class);

        String viewString = intent.getStringExtra("View");
        view = g.fromJson(viewString, OrderSheet.class);


        RecyclerView recyclerView = findViewById(R.id.orderProductsRv);
        TextView price = findViewById(R.id.priceText);
        price.setText(String.valueOf(view.getTotal()));

        displayedProducts = (ArrayList<Product>) view.getProducts();
        ProductAdapter adapter = new ProductAdapter(displayedProducts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);

    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            selectedProduct = displayedProducts.get(viewHolder.getAdapterPosition());

            RatingDialog ratingDialog = new RatingDialog(selectedProduct);
            ratingDialog.show(getSupportFragmentManager(), "Rating Dialog");


        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.logout){

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }

        if(item.getItemId()==R.id.home){
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                intent.putExtra("User", mUser);


                Gson g = new Gson();
                String orderString = g.toJson(cart);
                intent.putExtra("Order", orderString);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Administrator")) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void applyReview(double stars, String reviewString) {
        postedReview = new Review(user.getName(), selectedProduct.getName(), reviewString, stars, selectedProduct.getId(), user.getId());
        new PostReview().execute();
    }

    private class GetCustomerInfo extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/customerwebservice/customerId/"+userId)
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();



                Gson g = new Gson();
                user = g.fromJson(result, Customer.class);



            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {



        }
    }

    private class PostReview extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Gson g = new Gson();
            String json = g.toJson(postedReview);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, json);

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/reviewwebservice/post")
                    .post(body)
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                Log.i("Review Response", response.body().string());




            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            Toast.makeText(getApplicationContext(), "Review Posted.", Toast.LENGTH_SHORT).show();

        }
    }
}