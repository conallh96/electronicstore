package com.example.electronicstore;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{
    private List<OrderSheet> orders;

    long DURATION = 500;
    private boolean on_attach = true;
    private int selectedPos = RecyclerView.NO_POSITION;



    public OrderAdapter(List<OrderSheet> orders){
        this.orders = orders;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.rcview_row_order, parent, false);


        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderSheet order = orders.get(position);

        holder.itemView.setSelected(selectedPos == position);

        if(selectedPos == position)
            holder.itemView.setBackgroundColor(Color.GRAY);


        else
            holder.itemView.setBackgroundColor(Color.WHITE);


        String iDString = "Order ID: " + order.getId();
        holder.orderId.setText(iDString);

        String totalString = "Order Total: " + order.getTotal();
        holder.orderTotal.setText(totalString);

        String itemsString = "Order Size: " + order.getProducts().size() + " items";
        holder.items.setText(itemsString);

        setAnimation(holder.itemView, position);


    }

    @Override
    public int getItemCount() {
        return orders.size();
    }




    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 3 : (i * DURATION / 4));
        animator.setDuration(300);
        animatorSet.play(animator);
        animator.start();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView orderId;
        public TextView orderTotal;
        public TextView items;



        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            orderId = (TextView) itemView.findViewById(R.id.custNameTxt);
            items = (TextView) itemView.findViewById(R.id.emailTxt);
            orderTotal = (TextView) itemView.findViewById(R.id.ordersNumberTxt);

        }





        @Override
        public void onClick(View v) {

            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);

            OrderSheet current = orders.get(selectedPos);

        }


    }

}
