package com.example.electronicstore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import okhttp3.OkHttpClient;

public class ProductReviews extends AppCompatActivity {

    private FirebaseUser mUser;
    private int userId;
    private int productId;
    private String accountType;
    private OrderSheet cart = new OrderSheet();
    private ArrayList<Review> productReviews = new ArrayList<>();


    private ReviewAdapter adapter = new ReviewAdapter(productReviews);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_reviews);


        Intent intent = getIntent();

        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        productId = intent.getIntExtra("productId", 0);
        accountType = intent.getStringExtra("type");



        Gson g = new Gson();
        String orderString = intent.getStringExtra("Order");
        cart = g.fromJson(orderString, OrderSheet.class);

        new GetProductReviews().execute();



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.logout){

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }

        if(item.getItemId()==R.id.home){
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                intent.putExtra("User", mUser);


                Gson g = new Gson();
                String orderString = g.toJson(cart);
                intent.putExtra("Order", orderString);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Administrator")) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }




    private class GetProductReviews extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/reviewwebservice/productId/"+productId)
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();


                String[] reviewStrings = result.split("/");

                for (String reviewString : reviewStrings) {




                    try {
                        JSONObject json = new JSONObject(reviewString);

                        Gson g = new Gson();
                        Review theReview = g.fromJson(String.valueOf(json), Review.class);

                        productReviews.add(theReview);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            RecyclerView recyclerView = findViewById(R.id.reviewsRv);

            recyclerView.setAdapter(adapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

            RatingBar ratingBar = findViewById(R.id.ratingBar2);
            double total = 0.0;
            for (Review r : productReviews){
                total = total + r.getRating();
            }
            float rating = (float) (total/productReviews.size());
            ratingBar.setRating(rating);

        }
    }


}