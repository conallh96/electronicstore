package com.example.electronicstore;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{
    private List<Product> products;

    long DURATION = 500;
    private boolean on_attach = true;
    private int selectedPos = RecyclerView.NO_POSITION;



    public ProductAdapter(List<Product> products){
        this.products = products;
    }

    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.rcview_row, parent, false);


        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product product = products.get(position);

        holder.itemView.setSelected(selectedPos == position);

        if(selectedPos == position)
            holder.itemView.setBackgroundColor(Color.GRAY);


        else
            holder.itemView.setBackgroundColor(Color.WHITE);


        holder.name.setText(product.getName());
        String priceEdit = "€" + String.valueOf(product.getPrice());
        holder.price.setText(priceEdit);
        String catEdit = "Category: " + product.getCategory();
        holder.category.setText(catEdit);
        String fromProv = "From: " + product.getManufacturer();
        holder.provider.setText(fromProv);


        Bitmap bmp = BitmapFactory.decodeByteArray(product.getImage(), 0, product.getImage().length);
        holder.productImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, 50, 50, false));


        setAnimation(holder.itemView, position);


    }

    @Override
    public int getItemCount() {
        return products.size();
    }




    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 3 : (i * DURATION / 4));
        animator.setDuration(300);
        animatorSet.play(animator);
        animator.start();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public ImageView productImage;
        public TextView provider;
        public TextView price;
        public TextView category;




        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            name = (TextView) itemView.findViewById(R.id.custNameTxt);
            productImage = (ImageView) itemView.findViewById(R.id.productImage);
            price = (TextView) itemView.findViewById(R.id.emailTxt);
            provider = (TextView) itemView.findViewById(R.id.sourceTxt);
            category = (TextView) itemView.findViewById(R.id.ordersNumberTxt);

        }





        @Override
        public void onClick(View v) {

            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);

            Product current = products.get(selectedPos);



            Log.i("Clicked", current.toString());
        }


    }

}
