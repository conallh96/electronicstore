package com.example.electronicstore;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;


public class EditProductDialog extends AppCompatDialogFragment {

    private EditProductListener listener;
    private Product product;

    public EditProductDialog(Product product) {
        this.product = product;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.edit_product_dialog, null);

        if (!(product == null)) {

            EditText stockBox = view.findViewById(R.id.stockBox);
            stockBox.setText(String.valueOf(product.getStock()));
            ImageView img = view.findViewById(R.id.imageViewProduct);

            Bitmap bmp = BitmapFactory.decodeByteArray(product.getImage(), 0, product.getImage().length);


            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bmp);
            roundedBitmapDrawable.setCircular(true);
            img.setImageDrawable(roundedBitmapDrawable);

            Animation myFadeInAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.fadein);
            img.startAnimation(myFadeInAnimation);


            builder.setView(view)
                    .setTitle("Edit " + product.getName() + " Stock")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            listener.refreshRecycler();

                        }
                    })
                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            int stock = Integer.parseInt(stockBox.getText().toString());

                            product.setStock(stock);

                            listener.editProduct(product);
                        }
                    });



        }
            return builder.create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (EditProductListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement EditProductListener");
        }
    }

    public interface EditProductListener {
        void editProduct(Product product);
        void refreshRecycler();
    }
}
