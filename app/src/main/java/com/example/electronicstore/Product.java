package com.example.electronicstore;


import java.util.Arrays;

public class Product {


    private int id;
    private String name, manufacturer, category;

    private byte[] image;
    private double price;
    private int stock;


    public Product() {
        // TODO Auto-generated constructor stub
    }




    public Product(String name, String manufacturer, String category, byte[] image, double price, int stock) {
        super();
        this.name = name;
        this.manufacturer = manufacturer;
        this.category = category;
        this.image = image;
        this.price = price;
        this.stock = stock;
    }




    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getManufacturer() {
        return manufacturer;
    }


    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }


    public String getCategory() {
        return category;
    }


    public void setCategory(String category) {
        this.category = category;
    }


    public byte[] getImage() {
        return image;
    }


    public void setImage(byte[] image) {
        this.image = image;
    }


    public double getPrice() {
        return price;
    }


    public void setPrice(double price) {
        this.price = price;
    }


    public int getStock() {
        return stock;
    }


    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", category='" + category + '\'' +
                ", image=" + Arrays.toString(image) +
                ", price=" + price +
                ", stock=" + stock +
                '}';
    }
}
