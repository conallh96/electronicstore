package com.example.electronicstore;

public class Review {

	private int id;
	private String customerName;
	private String productName;
	private String reviewText;
	private double rating;
	private int productId;
	private int customerId;

	public Review() {

	}

	public Review(String customerName, String productName, String reviewText, double rating, int productId, int customerId) {
		super();
		this.customerName = customerName;
		this.productName = productName;
		this.reviewText = reviewText;
		this.rating = rating;
		this.productId = productId;
		this.customerId = customerId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getReviewText() {
		return reviewText;
	}

	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


}
