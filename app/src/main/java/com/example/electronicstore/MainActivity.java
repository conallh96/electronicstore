package com.example.electronicstore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private OrderSheet cart = new OrderSheet();
    private ArrayList<Product> storeProducts = new ArrayList<Product>();
    private ArrayList<Product> searchedProducts = new ArrayList<Product>();
    private ArrayList<Product> displayedProducts = new ArrayList<Product>();
    private ProductAdapter adapter = new ProductAdapter(displayedProducts);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if(intent.hasExtra("Order")){
            Gson g = new Gson();
            String orderString = intent.getStringExtra("Order");
            cart = g.fromJson(orderString, OrderSheet.class);
        }
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        cart.setCustomerId(userId);
        accountType = intent.getStringExtra("type");

        setSpinners();
        new GetStoreInfo().execute();
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            Product selected = storeProducts.get(viewHolder.getAdapterPosition());

            cart.addProduct(selected);

            Toast.makeText(getApplicationContext(), selected.getName() + " Added to Cart", Toast.LENGTH_SHORT).show();

            adapter.notifyDataSetChanged();

        }
    };

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            Product getReviews = storeProducts.get(viewHolder.getAdapterPosition());

            Intent intent = new Intent(getApplicationContext(), ProductReviews.class);


            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);

            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("productId", getReviews.getId());
            intent.putExtra("type", accountType);

            startActivity(intent);


        }
    };


    public void searchClick(View view){

        EditText term = findViewById(R.id.searchTerm);
        Spinner filterSpin = findViewById(R.id.spinner);
        String termString = term.getText().toString();
        String filterString = filterSpin.getSelectedItem().toString();
        searchedProducts.clear();

        if( termString.equals("") || filterString.equals("Select a Filter")){

            Toast.makeText(getApplicationContext(), "Please Enter Valid Search Parameters!", Toast.LENGTH_SHORT).show();

        }
        else{

            boolean results = false;
            switch(filterString)
            {
                case "Product Name":
                    for(Product p : storeProducts){
                        if (p.getName().contains(termString)){
                            results = true;
                            searchedProducts.add(p);
                        }
                    }
                    break;
                case "Manufacturer":
                    for(Product p : storeProducts){
                        if (p.getManufacturer().equals(termString)){
                            results = true;
                            searchedProducts.add(p);
                        }
                    }
                    break;
                case "Category":
                    for(Product p : storeProducts){
                        if (p.getCategory().contains(termString)){
                            results = true;
                            searchedProducts.add(p);
                        }
                    }
                    break;

            }

            if (results){
                displayedProducts.clear();
                displayedProducts.addAll(searchedProducts);
                adapter.notifyDataSetChanged();
                results=false;

            }
            else{

                Toast.makeText(getApplicationContext(), "No Products Matched Search!", Toast.LENGTH_SHORT).show();

            }

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void sortClick(View view){

        ArrayList<Product> sorted = new ArrayList<>();
        Spinner sortSpin = findViewById(R.id.spinner2);
        String sortString = sortSpin.getSelectedItem().toString();

        if( sortString.equals("Sort By")){

            Toast.makeText(getApplicationContext(), "Please Select Criteria To Sort By!", Toast.LENGTH_SHORT).show();

        }
        else {


            switch (sortString) {
                case "Price (High - Low)":

                    Collections.sort(displayedProducts, Comparator.comparingDouble(Product::getPrice));
                    Collections.reverse(displayedProducts);
                    adapter.notifyDataSetChanged();

                    break;
                case "Price (Low - High)":

                    Collections.sort(displayedProducts, Comparator.comparingDouble(Product::getPrice));
                    adapter.notifyDataSetChanged();

                    break;

                case "Manufacturer":

                    Collections.sort(displayedProducts, Comparator.comparing(Product::getManufacturer));
                    adapter.notifyDataSetChanged();

                    break;

            }
        }


    }

    public void setSpinners(){
        Spinner filterSpinner = findViewById(R.id.spinner);

        String[] filterItems = new String[]{"Select a Filter", "Product Name", "Category", "Manufacturer"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, filterItems);

        filterSpinner.setAdapter(adapter);

        Spinner sortSpinner = findViewById(R.id.spinner2);

        String[] sortItems = new String[]{"Sort By", "Price (High - Low)", "Price (Low - High)", "Manufacturer" };

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, sortItems);

        sortSpinner.setAdapter(adapter1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //add option menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        inflater.inflate(R.menu.cart, menu);
        inflater.inflate(R.menu.my_reviews, menu);
        inflater.inflate(R.menu.my_orders, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {


            Intent intent = new Intent(this, Login.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.myReviewsOption) {


            Intent intent = new Intent(this, MyReviews.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);
            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.myOrdersOption) {


            Intent intent = new Intent(this, MyOrders.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);
            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.cart) {


            Intent intent = new Intent(this, Cart.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);
            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    private class GetStoreInfo extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/productwebservice/products")
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();


                String[] productStrings = result.split("/");

                for (String productString : productStrings) {




                    try {
                        JSONObject json = new JSONObject(productString);

                        Gson g = new Gson();
                        Product theProduct = g.fromJson(String.valueOf(json), Product.class);

                        if(theProduct.getStock()>0) {
                            storeProducts.add(theProduct);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                displayedProducts.addAll(storeProducts);
                searchedProducts.addAll(storeProducts);
                Log.i("Products", storeProducts.toString());
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

            @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {


                RecyclerView recyclerView = findViewById(R.id.productRv);

                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
                new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(recyclerView);
                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
                recyclerView.setAdapter(adapter);



            }
    }

}