package com.example.electronicstore;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class Checkout extends AppCompatActivity {

    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private OrderSheet cart = new OrderSheet();
    private Customer user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Intent intent = getIntent();

        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        new GetCustomerInfo().execute();

        setSpinners();

        Gson g = new Gson();
        String orderString = intent.getStringExtra("Order");
        cart = g.fromJson(orderString, OrderSheet.class);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void makeOrder(View view){


        HashMap<Integer, Integer> orderAmounts = new HashMap<>();
        Boolean stockOk = true;


        for (Product p : cart.getProducts()) {



                if (orderAmounts.containsKey(p.getId())) {
                    orderAmounts.replace(p.getId(), orderAmounts.get(p.getId())+1);
                }
                else {
                    orderAmounts.put(p.getId(),1);
                }

            }


        Set entrySet = orderAmounts.entrySet();


        Iterator it = entrySet.iterator();


        while(it.hasNext()){
            Map.Entry<Integer,Integer> me = (Map.Entry<Integer, Integer>)it.next();


            for (Product p : cart.getProducts()) {
                if(p.getId()==me.getKey() && p.getStock()<me.getValue()){

                    stockOk=false;

                }
            }


        }


        if(!stockOk){
            Toast.makeText(getApplicationContext(), "No enough stock, please re-assess order.", Toast.LENGTH_SHORT).show();
        }
        else {
            new MakeOrder().execute();
        }
    }

    public void setCardDetails(){

        EditText cardEdit = findViewById(R.id.cardNoText);
        cardEdit.setText(user.getCardNo());
        cardEdit.setKeyListener(null);

        TextView cartTotal = findViewById(R.id.totalTxt);
        String newTotal = "Cart Total: €" + cart.getTotal();
        cartTotal.setText(newTotal);

        TextView cartSize = findViewById(R.id.sizeTxt);
        String newSize = "Items in Cart: " + cart.getProducts().size();
        cartSize.setText(newSize);


    }

    public void setSpinners(){

        Spinner dropdown = findViewById(R.id.spinnerMonth);

        String[] months = new String[]{"MM","01","02","03","04","05","06","07","08","09","10","11","12" };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, months);

        dropdown.setAdapter(adapter);

        Spinner dropdown1 = findViewById(R.id.spinnerYear);

        String[] years = new String[]{"YYYY","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032" };

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, years);

        dropdown1.setAdapter(adapter1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        getMenuInflater().inflate(R.menu.cart, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.logout){

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }

        if(item.getItemId()==R.id.home){
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                intent.putExtra("User", mUser);


                Gson g = new Gson();
                String orderString = g.toJson(cart);
                intent.putExtra("Order", orderString);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Administrator")) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        if (item.getItemId() == R.id.cart) {


            Intent intent = new Intent(this, Cart.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);
            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    private class GetCustomerInfo extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/customerwebservice/customerId/"+userId)
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();



                Gson g = new Gson();
                user = g.fromJson(result, Customer.class);



            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            setCardDetails();

        }
    }

    private class MakeOrder extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Gson g = new Gson();
            String json = g.toJson(cart);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, json);

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/orderwebservice/post")
                    .post(body)
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                Log.i("Order Response", response.body().string());



            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            Toast.makeText(getApplicationContext(), "Order Successful.", Toast.LENGTH_SHORT).show();

            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Administrator")) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }



        }
    }
}