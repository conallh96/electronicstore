package com.example.electronicstore;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder>{
    private List<Review> reviews;

    long DURATION = 500;
    private boolean on_attach = true;
    private int selectedPos = RecyclerView.NO_POSITION;



    public ReviewAdapter(List<Review> reviews){
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.rcview_row_review, parent, false);


        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Review review = reviews.get(position);

        holder.itemView.setSelected(selectedPos == position);

        if(selectedPos == position)
            holder.itemView.setBackgroundColor(Color.GRAY);


        else
            holder.itemView.setBackgroundColor(Color.WHITE);


        holder.name.setText(review.getProductName());
        holder.reviewerName.setText(review.getCustomerName());
        holder.rating.setRating((float)review.getRating());
        holder.reviewText.setText(review.getReviewText());



        setAnimation(holder.itemView, position);


    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }




    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 3 : (i * DURATION / 4));
        animator.setDuration(300);
        animatorSet.play(animator);
        animator.start();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public TextView reviewerName;
        public TextView reviewText;
        public RatingBar rating;






        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            name = (TextView) itemView.findViewById(R.id.productName);
            reviewerName = (TextView) itemView.findViewById(R.id.reviewersName);
            reviewText = (TextView) itemView.findViewById(R.id.reviewText);
            rating = (RatingBar) itemView.findViewById(R.id.ratingBar);


        }





        @Override
        public void onClick(View v) {

            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);

            Review current = reviews.get(selectedPos);



            Log.i("Clicked", current.toString());
        }


    }

}
