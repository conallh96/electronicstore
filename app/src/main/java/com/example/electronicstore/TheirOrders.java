package com.example.electronicstore;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;

public class TheirOrders extends AppCompatActivity {

    private FirebaseUser mUser;
    private int userId;
    private String searchId;
    private String accountType;
    private OrderSheet cart = new OrderSheet();
    private ArrayList<OrderSheet> myOrders = new ArrayList<>();

    private OrderAdapter adapter = new OrderAdapter(myOrders);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);


        Intent intent = getIntent();

        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");
        searchId = intent.getStringExtra("searchId");



        Gson g = new Gson();
        String orderString = intent.getStringExtra("Order");
        cart = g.fromJson(orderString, OrderSheet.class);



        new GetMyOrders().execute();



    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            OrderSheet order = myOrders.get(viewHolder.getAdapterPosition());

            Intent intent = new Intent(getApplicationContext(), ViewTheirOrder.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);

            String viewString = g.toJson(order);
            intent.putExtra("View", viewString);

            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);


        }
    };




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.logout){

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }

        if(item.getItemId()==R.id.home){
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                intent.putExtra("User", mUser);


                Gson g = new Gson();
                String orderString = g.toJson(cart);
                intent.putExtra("Order", orderString);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Administrator")) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }


    private class GetMyOrders extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/orderwebservice/customerId/"+searchId)
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();


                String[] reviewStrings = result.split("/");

                for (String reviewString : reviewStrings) {




                    try {
                        JSONObject json = new JSONObject(reviewString);

                        Gson g = new Gson();
                        OrderSheet theOrder = g.fromJson(String.valueOf(json), OrderSheet.class);

                        myOrders.add(theOrder);

                        Log.i("Order", theOrder.toString());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            RecyclerView recyclerView = findViewById(R.id.ordersRv);
            recyclerView.setAdapter(adapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
            new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(recyclerView);

        }
    }
}