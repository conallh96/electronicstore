package com.example.electronicstore;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class OrderSheet {


    private int id;
    private int customerId;
    private double total;
    List<Product> products = new ArrayList<Product>();

    public OrderSheet() {

    }



    public OrderSheet(int customerId, List<Product> products) {
        super();

        this.customerId = customerId;
        this.products = products;

        double cost = 0.0;
        for (Product product : products) {

            cost = cost + product.getPrice();

        }
        this.total=cost;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }


    public double getTotal() {
        double cost = 0.0;
        for (Product product : products) {

            cost = cost + product.getPrice();

        }

        DecimalFormat df = new DecimalFormat("####0.00");
        total = Double.parseDouble(df.format(cost));



        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProduct(Product product){
        products.add(product);
    }

    public void removeProduct(Product product){
        products.remove(product);
    }


    @Override
    public String toString() {
        return "OrderSheet{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", total=" + total +
                ", products=" + products +
                '}';
    }
}

