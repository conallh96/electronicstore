package com.example.electronicstore;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class SignUp extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mAuth = FirebaseAuth.getInstance();
        Spinner dropdown = findViewById(R.id.countySpinner);

        String[] counties = new String[]{"County", "Antrim", "Armagh","Carlow","Cavan","Clare","Cork","Derry","Donegal","Down","Dublin","Fermanagh","Galway",
                "Kerry","Kildare","Kilkenny","Laois","Leitrim","Limerick","Longford","Louth","Mayo","Meath","Monaghan","Offaly","Roscommon","Sligo",
                "Tipperary","Tyrone","Waterford","Westmeath","Wexford","Wicklow" };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, counties);

        dropdown.setAdapter(adapter);

        String[] types = new String[]{"Select Account Type", "Customer", "Administrator"};
        ArrayAdapter<String> adapterTypes = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, types);
        Spinner dropdownTypes = findViewById(R.id.typeSpinner);
        dropdownTypes.setAdapter(adapterTypes);
    }


    public void createUserWithEmailAndPassword(View view) {

        EditText name = (EditText) findViewById(R.id.textUsername);
        String nameString = name.getText().toString();

        EditText pass = (EditText) findViewById(R.id.textPassword);
        String passString = pass.getText().toString();

        EditText passConfirm = (EditText) findViewById(R.id.confirmPassword);
        String confirmString = passConfirm.getText().toString();

        EditText fullName = (EditText) findViewById(R.id.nameBox);
        String fullNameString = fullName.getText().toString();

        EditText address1 = (EditText) findViewById(R.id.addressBox1);
        String addressString1 = address1.getText().toString();

        EditText address2 = (EditText) findViewById(R.id.addressBox2);
        String addressString2 = address2.getText().toString();

        EditText eircode = (EditText) findViewById(R.id.eircodeBox);
        String eircodeString = eircode.getText().toString();

        EditText number = (EditText) findViewById(R.id.phoneBox);
        String phoneString = number.getText().toString();

        EditText card = (EditText) findViewById(R.id.cardNumber);
        String cardString = card.getText().toString();


        Spinner spinner = findViewById(R.id.countySpinner);
        String countyString = spinner.getSelectedItem().toString();

        Spinner spinnerType = findViewById(R.id.typeSpinner);
        String typeString = spinnerType.getSelectedItem().toString();

        if (passString.matches(confirmString)) {

            mAuth.createUserWithEmailAndPassword(nameString, passString)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d("myActivity", "createUserWithEmail:success");
                                mUser = mAuth.getCurrentUser();

                                
                                Customer customer = new Customer(fullNameString, mUser.getEmail(),addressString1,addressString2, countyString, eircodeString,  phoneString, cardString,typeString);




                                ObjectMapper mapper = new ObjectMapper();
                                //Converting the Object to JSONString
                                String jsonString = null;
                                try {
                                    jsonString = mapper.writeValueAsString(customer);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                JSONObject json = null;
                                try {
                                    json = new JSONObject(jsonString);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }



                                RequestQueue queue = Volley.newRequestQueue(SignUp.this);

                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                        Request.Method.POST, "http://localhost:8080/ElectronicStoreAPI/rest/customerwebservice/post", json, new
                                        Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {



                                                try {

                                                    String id = response.getString("id");;



                                                    if(response.get("type").equals("Customer")) {
                                                        Intent intent = new Intent(SignUp.this, MainActivity.class);
                                                        intent.putExtra("User", mUser);
                                                        intent.putExtra("id", Integer.valueOf(id));
                                                        intent.putExtra("type", response.get("type").toString());

                                                        Toast.makeText(SignUp.this, "Authentication Successful.", Toast.LENGTH_SHORT).show();

                                                        startActivity(intent);
                                                    }
                                                    if(response.get("type").equals("Administrator")){
                                                        Intent intent = new Intent(SignUp.this, MainActivityAdmin.class);
                                                        intent.putExtra("User", mUser);
                                                        intent.putExtra("id", Integer.valueOf(id));
                                                        intent.putExtra("type", response.get("type").toString());

                                                        Toast.makeText(SignUp.this, "Authentication Successful.", Toast.LENGTH_SHORT).show();

                                                        startActivity(intent);
                                                    }


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }


                                            }
                                        }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {


                                        Log.i("DB Failed", error.getMessage());
                                        Toast.makeText(SignUp.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    }


                                }
                                );

                                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                queue.add(jsonObjectRequest);





                                //code for details

                            } else {
                                Log.w("myActivity", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(SignUp.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });

        }

        else if (!passString.matches(confirmString)){

            Toast.makeText(SignUp.this, "Passwords Do No Match!.", Toast.LENGTH_SHORT).show();

        }

    }


}