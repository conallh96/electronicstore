package com.example.electronicstore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;

public class Cart extends AppCompatActivity {


    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private OrderSheet cart = new OrderSheet();
    private ArrayList<Product> cartProducts = new ArrayList();
    private Customer user;

    private ProductAdapter adapter = new ProductAdapter(cartProducts);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Intent intent = getIntent();

        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        accountType = intent.getStringExtra("type");

        new GetCustomerInfo().execute();

        Gson g = new Gson();
        String orderString = intent.getStringExtra("Order");
        cart = g.fromJson(orderString, OrderSheet.class);
        cartProducts.addAll(cart.getProducts());

        RecyclerView recyclerView = findViewById(R.id.productsRv);

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(recyclerView);

        updateTotal();



    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            Product selected = cart.getProducts().get(viewHolder.getAdapterPosition());

            cartProducts.remove(selected);

            cart.setProducts(cartProducts);

            Toast.makeText(getApplicationContext(), selected.getName() + " Removed Cart", Toast.LENGTH_SHORT).show();

            adapter.notifyDataSetChanged();

            updateTotal();

        }
    };

    public void clickCheckout(View view){

        Intent intent = new Intent(getApplicationContext(), Checkout.class);

        intent.putExtra("User", mUser);


        Gson g = new Gson();
        cart.setProducts(cartProducts);
        String orderString = g.toJson(cart);
        intent.putExtra("Order", orderString);

        intent.putExtra("id", userId);

        intent.putExtra("type",accountType);

        startActivity(intent);

    }


    public void updateTotal(){
        TextView cartTotal = findViewById(R.id.cartTotal);
        String newTotal = "Cart Total: €" + cart.getTotal();
        cartTotal.setText(newTotal);
    }

    public void setShipping(){

        String shipping = user.getName()+",\n"+user.getAddress1()+",\n"+user.getAddress2()+",\n"+user.getEircode()+",\n"
                +"Co."+user.getCounty();
        TextView shipTxt = findViewById(R.id.shippingDetails);
        shipTxt.setText(shipping);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        getMenuInflater().inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.logout){

            Intent intent = new Intent(this, Login.class);


            finish();

            startActivity(intent);

        }

        if(item.getItemId()==R.id.home){
            if (accountType.equals("Customer")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                intent.putExtra("User", mUser);


                Gson g = new Gson();
                cart.setProducts(cartProducts);
                String orderString = g.toJson(cart);
                intent.putExtra("Order", orderString);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

            if (accountType.equals("Administrator")) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);

                intent.putExtra("User", mUser);

                intent.putExtra("id", userId);

                intent.putExtra("type",accountType);

                startActivity(intent);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private class GetCustomerInfo extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/customerwebservice/customerId/"+userId)
                    .get()
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();



                        Gson g = new Gson();
                        user = g.fromJson(result, Customer.class);



                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            setShipping();

        }
    }

}