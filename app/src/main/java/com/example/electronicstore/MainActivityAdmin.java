package com.example.electronicstore;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class MainActivityAdmin extends AppCompatActivity  implements EditProductDialog.EditProductListener {

    private FirebaseUser mUser;
    private int userId;
    private String accountType;
    private Product updateStock;
    private OrderSheet cart = new OrderSheet();
    private ArrayList<Product> storeProducts = new ArrayList<Product>();
    private ArrayList<Product> searchedProducts = new ArrayList<Product>();
    private ArrayList<Product> displayedProducts = new ArrayList<Product>();
    private ProductAdapter adapter = new ProductAdapter(displayedProducts);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);

        Intent intent = getIntent();
        if (intent.hasExtra("Order")) {
            Gson g = new Gson();
            String orderString = intent.getStringExtra("Order");
            cart = g.fromJson(orderString, OrderSheet.class);
        }
        mUser = intent.getParcelableExtra("User");
        userId = intent.getIntExtra("id", 0);
        cart.setCustomerId(userId);
        accountType = intent.getStringExtra("type");

        setSpinners();
        new GetStoreInfo().execute();
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            Product selected = storeProducts.get(viewHolder.getAdapterPosition());


            EditProductDialog editDialog = new EditProductDialog(selected);
            editDialog.show(getSupportFragmentManager(), "Edit Product Dialog");

        }
    };

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            Product getReviews = storeProducts.get(viewHolder.getAdapterPosition());

            Intent intent = new Intent(getApplicationContext(), ProductReviews.class);


            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);

            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("productId", getReviews.getId());
            intent.putExtra("type", accountType);

            startActivity(intent);


        }
    };


    public void searchClick(View view) {

        EditText term = findViewById(R.id.searchTerm);
        Spinner filterSpin = findViewById(R.id.spinner);
        String termString = term.getText().toString();
        String filterString = filterSpin.getSelectedItem().toString();
        searchedProducts.clear();

        if (termString.equals("") || filterString.equals("Select a Filter")) {

            Toast.makeText(getApplicationContext(), "Please Enter Valid Search Parameters!", Toast.LENGTH_SHORT).show();

        } else {

            boolean results = false;
            switch (filterString) {
                case "Product Name":
                    for (Product p : storeProducts) {
                        if (p.getName().contains(termString)) {
                            results = true;
                            searchedProducts.add(p);
                        }
                    }
                    break;
                case "Manufacturer":
                    for (Product p : storeProducts) {
                        if (p.getManufacturer().equals(termString)) {
                            results = true;
                            searchedProducts.add(p);
                        }
                    }
                    break;
                case "Category":
                    for (Product p : storeProducts) {
                        if (p.getCategory().contains(termString)) {
                            results = true;
                            searchedProducts.add(p);
                        }
                    }
                    break;

            }

            if (results) {
                displayedProducts.clear();
                displayedProducts.addAll(searchedProducts);
                adapter.notifyDataSetChanged();
                results = false;

            } else {

                Toast.makeText(getApplicationContext(), "No Products Matched Search!", Toast.LENGTH_SHORT).show();

            }

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void sortClick(View view) {

        ArrayList<Product> sorted = new ArrayList<>();
        Spinner sortSpin = findViewById(R.id.spinner2);
        String sortString = sortSpin.getSelectedItem().toString();

        if (sortString.equals("Sort By")) {

            Toast.makeText(getApplicationContext(), "Please Select Criteria To Sort By!", Toast.LENGTH_SHORT).show();

        } else {


            switch (sortString) {
                case "Price (High - Low)":

                    Collections.sort(displayedProducts, Comparator.comparingDouble(Product::getPrice));
                    Collections.reverse(displayedProducts);
                    adapter.notifyDataSetChanged();

                    break;
                case "Price (Low - High)":

                    Collections.sort(displayedProducts, Comparator.comparingDouble(Product::getPrice));
                    adapter.notifyDataSetChanged();

                    break;

                case "Manufacturer":

                    Collections.sort(displayedProducts, Comparator.comparing(Product::getManufacturer));
                    adapter.notifyDataSetChanged();

                    break;

            }
        }


    }

    public void setSpinners() {
        Spinner filterSpinner = findViewById(R.id.spinner);

        String[] filterItems = new String[]{"Select a Filter", "Product Name", "Category", "Manufacturer"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, filterItems);

        filterSpinner.setAdapter(adapter);

        Spinner sortSpinner = findViewById(R.id.spinner2);

        String[] sortItems = new String[]{"Sort By", "Price (High - Low)", "Price (Low - High)", "Manufacturer"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, sortItems);

        sortSpinner.setAdapter(adapter1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //add option menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        inflater.inflate(R.menu.customers, menu);
        inflater.inflate(R.menu.add_product, menu);


        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logout) {


            Intent intent = new Intent(this, Login.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.customers) {


            Intent intent = new Intent(this, ViewCustomers.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);
            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.addProduct) {


            Intent intent = new Intent(this, AddProduct.class);

            Gson g = new Gson();
            String orderString = g.toJson(cart);
            intent.putExtra("Order", orderString);
            intent.putExtra("User", mUser);
            intent.putExtra("id", userId);
            intent.putExtra("type", accountType);

            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void editProduct(Product product) {
        Log.i("New Stock", String.valueOf(product.getStock()));
        adapter.notifyDataSetChanged();
        updateStock = product;
        new UpdateStock().execute();

    }

    @Override
    public void refreshRecycler() {
        adapter.notifyDataSetChanged();
    }

    private class UpdateStock extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {

            OkHttpClient client = new OkHttpClient();

            Gson g = new Gson();
            String json = g.toJson(updateStock);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, json);

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("http://localhost:8080/ElectronicStoreAPI/rest/productwebservice/update/"+updateStock.getId())
                    .put(body)
                    .build();


            try {
                okhttp3.Response response = client.newCall(request).execute();
                assert response.body() != null;
                Log.i("Order Response", response.body().string());


            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Void result) {

            Toast.makeText(getApplicationContext(), "Stock Update Successful.", Toast.LENGTH_SHORT).show();



        }

    }

        private class GetStoreInfo extends AsyncTask<Void, Void, Void> {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            protected Void doInBackground(Void... voids) {

                OkHttpClient client = new OkHttpClient();

                okhttp3.Request request = new okhttp3.Request.Builder()
                        .url("http://localhost:8080/ElectronicStoreAPI/rest/productwebservice/products")
                        .get()
                        .build();


                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    assert response.body() != null;
                    String result = response.body().string();


                    String[] productStrings = result.split("/");

                    for (String productString : productStrings) {


                        try {
                            JSONObject json = new JSONObject(productString);

                            Gson g = new Gson();
                            Product theProduct = g.fromJson(String.valueOf(json), Product.class);

                            storeProducts.add(theProduct);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    displayedProducts.addAll(storeProducts);
                    searchedProducts.addAll(storeProducts);
                    Log.i("Products", storeProducts.toString());
                    return null;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected void onPostExecute(Void result) {


                RecyclerView recyclerView = findViewById(R.id.productRv);

                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
                new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(recyclerView);
                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
                recyclerView.setAdapter(adapter);


            }
        }

    }
